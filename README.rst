Tinymatron
==========

NodeJS should not be required (pending commit of js/lodash.js). Supplementing with a full build (can be downloaded from the site) should suffice

Any dependencies are NOT to be pulled in with npm/bower but committed directly.

TODO: Split 3rd party files into separate "vendor/" directory (or something idgaf)

Everything should be reasonably self-explanatory for both developers and end users

Project is in the earliest pre-alpha phase, so not for the faint of heart

Coding style is basically 2-spaces indent, all optional semicolons omitted.
Indentation style may differ here and there, it's not important at this stage
of development

Core JS functionality should be separable from Firefox at some point, but as of
this writing it's only been tested in FF31

Compatibility for 1.0 is being aimed at Chrome, FF3.5+, IE9+, various versions of Epiphany and/or Midori

All of the above is subject to change. The license might change to MIT or Zlib at some point, all other (useless) commits remain WTFPL except where otherwise noted in the various 3rd-party source files
