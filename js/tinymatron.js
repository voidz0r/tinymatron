/* vim: set ts=2 sw=2 et: */
define("tinymatron", ["lodash"], function(Ld) {
"use strict"

function Tinymatron(config, canvas) {
  function Animation(fn, start, end, repeat) {
    if (typeof repeat === "undefined") {
      repeat = INF
    }
    if (repeat > 254) {
      repeat = INF
    }
    if (repeat < 0) {
      repeat = 0
    }

    this.steps = Math.abs(end - start)
    this.step = start
    this.end = end
    this.tick = fn.bind(this)
    this.repeat = repeat // not implemented
  }

  function Render(fn, width, height) {
    this.fn = fn.bind(this)
    this.width = width
    this.height = height
  }

  var _buffer, _config, _canvas, _context

  this.setConfig = function(config) {
    _config = Ld.defaults(config, {
      "matrix": {
        "w": 5,
        "h": 7
      },
      "led": {
        "w": 64,
        "h": 64,
      },
      "margin": {
        "w": 80,
        "h": 80
      }
    })
  }

  this.setCanvas = function(element) {
    _canvas = element
    _context = element.getContext("2d")
  }

  var animations = {
    //"forward": function() {
    //  this.step++
    //},
    "once": function() {
      if (this.start < this.end) {
        if (this.step < this.end) {
          this.step++
        }
      }
      else {
        if (this.step < this.start) {
          this.step--
        }
      }
    },
    "leapfrog": function() {
      if (typeof this.state === "undefined") {
        this["state"] = false
      }
      var dir = this.state ? 2 : -1

      this.step += dir
      if (this.step > this.end) {
        this.step = this.start
      }
    },
    "loop": function() {
      this.step++
      if (this.step > this.end) {
        this.step = this.start
      }
    },
    "pingpong": function() {
      if (typeof this.dir === "undefined") {
        this["dir"] = 1
      }
      this.step += this.dir
      if ((this.step < start) || (this.step > end)) {
        this.dir = 0 - this.dir
      }
    },
    "random": function() {
      this.step = Ld.random(this.start, this.end)
    },
    "reverse": function() {
      this.step--
      if (this.step < this.start) {
        this.step = this.end
      }
    },
    "static": Ld.noop
  }

  var renderers = {
    "random": function(m) {
      m.map(function() {
        return !!Math.round(Math.random())
      })
    },
    "allon": function(m) {
      m.map(Ld.constant(false))
    },
    "alloff": function(m) {
      m.map(Ld.constant(true))
    }
  }

  this.clear = function() {
    _buffer.fill(false) // FF31+

    return this
  }

  this.init = function() {
    _canvas.width = (config.led.w + config.margin.w) * config.matrix.w
    _canvas.height = (config.led.h + config.margin.h) * config.matrix.h

    var context = _canvas.getContext("2d")
    var ctx = context

    //context.beginPath()

    var x = 0, y = 0, w = 64, h = 64
    var kappa = .5522848,
        ox = (w / 2) * kappa, // control point offset horizontal
        oy = (h / 2) * kappa, // control point offset vertical
        xe = x + w,           // x-end
        ye = y + h,           // y-end
        xm = x + w / 2,       // x-middle
        ym = y + h / 2;       // y-middle

    var cw = _canvas.width
    var ch = _canvas.height
    var mw = _config.margin.w
    var mh = _config.margin.h

    ctx.beginPath();
    ctx.moveTo(0, 0)
    ctx.bezierCurveTo(cw, 0)
    ctx.bezierCurveTo(cw, ym, cw, ym, cw, ym)

    ctx.bezierCurveTo(

    //ctx.moveTo(x, ym);
    ctx.bezierCurveTo(x, ym - oy, xm - ox, y, xm, y);
    //ctx.bezierCurveTo(xm + ox, y, xe, ym - oy, xe, ym);
    //ctx.bezierCurveTo(xe, ym + oy, xm + ox, ye, xm, ye);
    //ctx.bezierCurveTo(xm - ox, ye, x, ym + oy, x, ym);
    //context.closePath()
    /*
    Ld.forEach(Ld.range(config.matrix.h), function(y) {
      y++
      Ld.forEach(Ld.range(config.matrix.w), function(x) {
        x++
        context.arc(
          ((_config.led.w + config.margin.w) * x) - (_config.led.w/2),
          ((_config.led.h + config.margin.h) * y) - (_config.led.h/2),
          _config.led.w,
          0,
          Math.PI*2,
          true
        )
        context.closePath()
      })
    })
    */
    //context.fillRect(0, 0, _canvas.width, _canvas.height)
    //context.closePath()
    context.strokeStyle = "#0F0"
    context.stroke()
    //context.clearRect(0, 0, _canvas.width, _canvas.height)

    /*
    var width = _canvas.width
    var height = _canvas.height
    context.beginPath();
    context.moveTo(width/2 - height/2, 0);
    context.lineTo(width/2 + height/2, height);
    context.lineWidth = 3;
    context.strokeStyle = "#0f0";
    context.stroke();

    // draw a circle with a hole in it
    context.beginPath();
    context.arc(width/2, height/2, height*3/8, 0, 2*Math.PI);
    context.arc(width/2, height/2, height*1/8, 0, 2*Math.PI, true);
    context.fillStyle = "#555";
    context.fill();
    */

    _buffer  = Ld.range(_config.matrix.w * _config.matrix.h)
    _context = context

    return this.clear()
  }

  if (typeof config === "undefined") {
    config = {}
  }
  if (typeof canvas === "undefined") {
    canvas = null
  }

  this.setConfig(config)
  try {
    this.setCanvas(canvas)
  } catch(e) {}
  this.init()
}

return Tinymatron
})
