/* vim: set ts=2 sw=2 et: */
+function() {
  "use strict"

  // Workaround for lodash
  if (typeof this.define.amd !== "object") {
    this.define["amd"] = {}
  }

	var print = function(msg) {
		if (typeof this.console !== "undefined") {
			this.console.log(msg);
		}
	}.bind(this)

  require(["contentLoaded", "tinymatron"], function(onReady, Tinymatron) {
    var d = document;

    onReady(this, function() {
      print("Loading... ")

      var elMain = d.getElementById("cvMain")
      var tiny = new Tinymatron({}, elMain)

      print("success\n")
    })
  }.bind(this))
}.call(window)
